from django.db import models

# Create your models here.


class ToDoList(models.Model):
    name = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class ToDoItem(models.Model):
    name = models.CharField(max_length=50,default='item')
    description = models.TextField(max_length=1000)
    due_date = models.DateField(blank=True,null=True)
    completed = models.BooleanField()

    todolist = models.ForeignKey(ToDoList,on_delete=models.CASCADE)

    def __str__(self):
        return self.name
