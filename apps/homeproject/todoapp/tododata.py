import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'homeproject.settings')
django.setup()
from todoapp.models import *
def main():
    for i in range(1,6):
        todolist = ToDoList()
        todolist.name = 'list'+str(i)
        todolist.save()
        for j in range(1,6):
            todoitem = ToDoItem()
            todoitem.name = "item"+str(j)
            todoitem.description = "Description in list"+str(i)+'.item'+str(j)
            todoitem.completed=True

            todoitem.todolist = todolist
            todoitem.save()

if __name__ == '__main__':
    main()
