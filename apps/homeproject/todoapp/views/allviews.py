from django.shortcuts import render
from django.views import View

from todoapp.models import ToDoList, ToDoItem


class ListView(View):
    def get(self,request):
        lists = ToDoList.objects.all()

        return render(
            request,
            template_name = 'lists.html',
            context = {
                'lists':lists,
            }
        )

class ItemView(View):
    def get(selfs,request,*args,**kwargs):
        items = ToDoItem.objects.filter(**kwargs)

        return render(
            request,
            template_name='lists.html',
            context = {
                'lists':items,
            }
        )