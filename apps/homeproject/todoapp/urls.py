from django.urls import path
from todoapp import views
urlpatterns = [
    path('lists/',views.ListView.as_view(),name='lists.url'),
    path('lists/<int:todolist_id>/',views.ItemView.as_view(),name='items.url'),
]