from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.db.models import Count
from django.shortcuts import render, redirect
from django.views import View

from seasonsapp.forms import LoginForm, SignupForm
from seasonsapp.models import Matches, Details, Image


class SeasonsView(View):
    def get(self,request,*args,**kwargs):
        seasons = [i['season'] for i in Matches.objects.values('season').distinct()]

        matches = Matches.objects.filter(**kwargs)
        paginator = Paginator(matches, 8)  # Show 8 matches per page

        page = request.GET.get('page')
        page_matches = paginator.get_page(page)
        image="dummy"
        if request.user.is_authenticated:
            image=Image.objects.get(user=request.user).profile_pic.url
        return render(
            request,
            template_name='seasons.html',
            context={
                'seasons':seasons,
                'matches':page_matches,
                'img_url':image,
            }
        )

class MatchView(View):
    def get(self,request,*args,**kwargs):
        if not request.user.is_authenticated:
            return redirect('login.url')
        match = Matches.objects.get(**kwargs)
        details = Details.objects.filter(match_id=match)
        top_3_batsmen = details.values('batsman', 'batting_team').annotate(runs=Count('batsman_runs')).order_by('-runs')[:3]
        top_3_bowlers = details.exclude(plaer_dismissed='').values('bowler','bowling_team').annotate(wickets=Count('bowler')).order_by('-wickets')[:3]
        return render(
            request,
            template_name="match.html",
            context={
                'match':match,
                'details':details,
                'top_3_batsmen':top_3_batsmen,
                'top_3_bowlers':top_3_bowlers,
            }
        )

class LoginView(View):
    def get(self,request):
        if request.user.is_authenticated:
            return redirect("season_home.url")
        login_form=LoginForm()

        return render(
            request,
            template_name='sign_in_up.html',
            context={
                'form':login_form,
                'act_as':'login',
            }
        )

    def post(self,request):
        user=authenticate(
            request,
            username=request.POST['user_name'],
            password=request.POST['password'],
        )

        if user is not None:
            login(request,user)
            return redirect("season_home.url")
        else:
            messages.error(request,"wrong credentials")
            return render(
                request,
                template_name="sign_in_up.html",
                context={
                    'form':LoginForm(request.POST),
                    'act_as':'login',
                }
            )

class SignupView(View):
    def get(self,request):
        signup_form=SignupForm()

        return render(
            request,
            template_name='sign_in_up.html',
            context={
                'form': signup_form,
                'act_as':'signup',
            }
        )

    def post(self,request):
        signup_form=SignupForm(request.POST,request.FILES)
        if signup_form.is_valid():
            image=signup_form.cleaned_data.pop('image')
            user=User.objects.create_user(**signup_form.cleaned_data)
            user.save()
            image_model_instance = Image()
            image_model_instance.profile_pic = image
            image_model_instance.user=user
            image_model_instance.save()
            return redirect("login.url")
        else:
            return render(
                request,
                template_name='sign_in_up.html',
                context={
                    'form': signup_form,
                    'act_as': 'signup',
                }
            )

def logout_fun(request):
    logout(request)
    return redirect("season_home.url")
