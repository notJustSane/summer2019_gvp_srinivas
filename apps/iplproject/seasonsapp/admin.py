from django.contrib import admin

# Register your models here.
from seasonsapp.models import *

admin.site.register(Matches)
admin.site.register(Details)
admin.site.register(Image)