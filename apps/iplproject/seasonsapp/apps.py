from django.apps import AppConfig


class SeasonsappConfig(AppConfig):
    name = 'seasonsapp'
