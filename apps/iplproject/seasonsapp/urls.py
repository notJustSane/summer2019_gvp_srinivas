from django.conf.urls.static import static
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, include

from iplproject import settings
from seasonsapp import views
urlpatterns=[
    path('seasons/<int:season>/',views.SeasonsView.as_view(),name='seasons.url'),
    path('seasons/',lambda x: redirect('seasons.url',season=2019) ,name='season_home.url'),
    path('match/<int:match_id>/',views.MatchView.as_view(),name='match.url'),
    path('login/',views.LoginView.as_view(),name='login.url'),
    path('logout/',views.logout_fun,name='logout.url'),
    path('signup/',views.SignupView.as_view(),name='signup.url'),
]

