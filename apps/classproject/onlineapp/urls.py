from django.urls import path
from django.conf.urls import url
from onlineapp import views
from rest_framework.authtoken import views as authview

urlpatterns = [
    #path('get_my_college/', views.get_my_colege),
    #path('student_info/<int:id>',views.students),
    path('colleges/',views.CollegeView.as_view(),name="college.url"),
    path('colleges/<int:id>',views.CollegeView.as_view(),name="college_detail.url"),
    path('colleges/<str:acronym>',views.CollegeView.as_view(),name="college_detail.url"),

    path('add_college/',views.AddCollegeView.as_view(),name="college_add.url"),
    path('edit_college/<int:id>/',views.AddCollegeView.as_view(),name="edit.url"),
    path('delete_college/<int:id>/',views.AddCollegeView.as_view(),name="delete.url"),

    path('colleges/<int:id>/add_student/',views.AddStudentView.as_view(),name='student_add.url'),
    path('edit_student/<int:id>/',views.AddStudentView.as_view(),name='student_edit.url'),
    path('delete_student/<int:id>/',views.AddStudentView.as_view(),name='student_delete.url'),

    path('login/',views.LoginView.as_view(),name='login.url'),
    path('signup/',views.SignupView.as_view(),name='signup.url'),
    path('logout/',views.logout_user,name="logout.url"),

    #rest urls starts

    path('api/colleges/',views.api_college_fun_view,name='api_colleges.url'),
    path('api/colleges/<int:id>/',views.api_college_detail_fun_view,name='api_college_detail.url'),
    path('api/colleges/<int:college_id>/students/',views.Api_student_class_view.as_view(),name='api_students.url'),
    path('api/colleges/<int:college_id>/students/<int:student_id>/',views.Api_student_edit_class_view.as_view(),name='api_student_detail.url'),
    #rest token generation url
    url(r'^api-token-auth/', authview.obtain_auth_token),

    #temp urls
    path('test/',views.testview),
]

