from django import forms

class LoginForm(forms.Form):

    user_name=forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class':'input','placeholder':'Enter Username'})
    )

    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class':'input','placeholder': 'Enter password'})
    )

class SignupForm(forms.Form):
    first_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class':'input','placeholder': 'Enter First_name'})
    )

    last_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class':'input','placeholder': 'Enter Last_name'})
    )

    username = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class':'input','placeholder': 'Enter User_name'})
    )

    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class':'input','placeholder': 'Enter password'})
    )
