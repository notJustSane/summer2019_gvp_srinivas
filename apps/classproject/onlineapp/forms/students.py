from onlineapp.models import Student,MockTest1
from django import forms

class AddStudent(forms.ModelForm):
    class Meta:
        model=Student
        exclude=['id','college']
        widgets={
            'name':forms.TextInput(attrs={'class':'input','placeholder':'Enter Name'}),
            'dob':forms.DateInput(attrs={'class':'input','placeholder':'mm/dd/yyyy'}),
            'email':forms.EmailInput(attrs={'class':'input','placeholder':'Enter Email'}),
            'db_folder':forms.TextInput(attrs={'class':'input','placeholder':'Enter Db Name'}),
            'dropped_out':forms.CheckboxInput(),
        }

class AddMarks(forms.ModelForm):
    class Meta:
        model=MockTest1
        exclude=['id','total','student']
        widgets={
            'problem1':forms.NumberInput(attrs={'class':'input','placeholder':'Enter Marks'}),
            'problem2':forms.NumberInput(attrs={'class':'input','placeholder':'Enter Marks'}),
            'problem3':forms.NumberInput(attrs={'class':'input','placeholder':'Enter Marks'}),
            'problem4':forms.NumberInput(attrs={'class':'input','placeholder':'Enter Marks'}),
        }
