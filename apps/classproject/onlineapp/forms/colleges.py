from onlineapp.models import College
from django import forms

class AddCollege(forms.ModelForm):
    class Meta:
        model=College
        exclude= ['id']
        widgets = {
            'name':forms.TextInput(attrs={'class':'input','placeholder':'Enter Name'}),
            'location':forms.TextInput(attrs={'class':'input','placeholder':'Enter Location'}),
            'acronym':forms.TextInput(attrs={'class':'input','placeholder':'Enter Acronym'}),
            'contact':forms.EmailInput(attrs={'class':'input','placeholder':'Enter Email'}),
        }


