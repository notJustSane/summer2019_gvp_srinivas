from django.contrib import admin

# Register your models here.
from .models import College
admin.site.register(College)

from .models import Student
admin.site.register(Student)

from .models import Teacher
admin.site.register(Teacher)

from .models import MockTest1
admin.site.register(MockTest1)