from django.http import HttpResponse

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from django.urls import resolve
from django.views import View
from django.contrib.auth import authenticate,login,logout

from onlineapp.models import *
from django.shortcuts import render,get_object_or_404,redirect
from onlineapp.forms import *
class CollegeView(LoginRequiredMixin,View):
    login_url = '/login/'
    def get(self,request,*args,**kwargs):
        if kwargs:
            #college = College.objects.get(id=kwargs['id'])
            college=get_object_or_404(College,**kwargs)
            students = Student.objects.filter(college=college)
            return render(
                request,
                template_name='students.html',
                context={
                    'permissions':request.user.get_all_permissions(),
                    'college':college,
                    'students': students,
                },
            )
        colleges=College.objects.all()
        return render(
            request,
            template_name="colleges.html",
            context={
                'permissions':request.user.get_all_permissions(),
                'colleges':colleges,
            }
        )
class StudentView(View):
    def get(self,request,id):
        college = College.objects.get(id=id)
        students = Student.objects.filter(college=college)
        return render(request, 'students.html', {'students': students})


class AddCollegeView(LoginRequiredMixin,PermissionRequiredMixin,View):
    login_url = '/login/'
    permission_required = 'onlineapp.add_college'
    def get(self,request,*args,**kwargs):
        if kwargs:
            if resolve(request.path_info).url_name == 'edit.url':
                college=College.objects.get(**kwargs)
                form=AddCollege(instance=college)
                return render(
                    request,
                    template_name='base.html',
                    context={
                        'form': form,
                    }
                )
            elif resolve(request.path_info).url_name == 'delete.url':
                College.objects.get(**kwargs).delete()
                return redirect("college.url")

        form=AddCollege()
        return render(
            request,
            template_name='base.html',
            context={
                'form':form,
            }
        )


    def post(self,request,*args,**kwargs):
        if kwargs:
            college=College.objects.get(**kwargs)
            form=AddCollege(request.POST,instance=college)
        else:
            form=AddCollege(request.POST)
        if form.is_valid():
            form.save()
            return redirect("college.url")

        return render(
            request,
            template_name="base.html",
            context={
                'form':form,
            }
        )


class AddStudentView(LoginRequiredMixin,View):
    login_url = '/login/'
    def get(self,request,*args,**kwargs):

        if kwargs:
            if resolve(request.path_info).url_name == "student_edit.url":
                student_form=AddStudent(instance=Student.objects.get(**kwargs))
                marks_form=AddMarks(instance=MockTest1.objects.get(student_id=kwargs['id']))
                return render(
                    request,
                    template_name="add_student.html",
                    context={
                        'student_form': student_form,
                        'marks_form': marks_form,
                    }
                )
            elif resolve(request.path_info).url_name == "student_delete.url":
                Student.objects.get(**kwargs).delete()
                return redirect('college.url')

        student_form=AddStudent()
        marks_form=AddMarks()
        return render(
            request,
            template_name="add_student.html",
            context={
                'student_form':student_form,
                'marks_form':marks_form,
            }
        )

    def post(self,request,*args,**kwargs):
        if resolve(request.path_info).url_name == "student_edit.url":
            student_form = AddStudent(request.POST,instance=Student.objects.get(**kwargs))
            marks_form = AddMarks(request.POST,instance=MockTest1.objects.get(student_id=kwargs['id']))

            s = student_form.save(commit=False)
            s.save()

            m = marks_form.save(commit=False)
            m.total = m.problem1 + m.problem2 + m.problem3 + m.problem4
            m.student = s
            m.save()
            return redirect("college.url")
        else:
            student_form=AddStudent(request.POST)
            marks_form=AddMarks(request.POST)

        if student_form.is_valid() and marks_form.is_valid():
            s=student_form.save(commit=False)
            s.college=College.objects.get(**kwargs)
            s.save()

            m=marks_form.save(commit=False)
            m.total=m.problem1+m.problem2+m.problem3+m.problem4
            m.student=s
            m.save()
            return redirect("college.url")



class LoginView(View):
    def get(self,request):
        if request.user.is_authenticated:
            return redirect("college.url")
        login_form=LoginForm()

        return render(
            request,
            template_name='sign_in_up.html',
            context={
                'form':login_form,
                'act_as':'login',
            }
        )

    def post(self,request):
        user=authenticate(
            request,
            username=request.POST['user_name'],
            password=request.POST['password'],
        )

        if user is not None:
            login(request,user)
            return redirect("college.url")
        else:
            messages.error(request,"wrong credentials")
            return render(
                request,
                template_name="sign_in_up.html",
                context={
                    'form':LoginForm(request.POST),
                    'act_as':'login',
                }
            )
class SignupView(View):
    def get(self,request):
        signup_form=SignupForm()

        return render(
            request,
            template_name='sign_in_up.html',
            context={
                'form': signup_form,
                'act_as':'signup',
            }
        )


    def post(self,request):
        signup_form=SignupForm(request.POST)
        if signup_form.is_valid():
            user=User.objects.create_user(**signup_form.cleaned_data)
            user.save()
            return redirect("login.url")
        else:
            return render(
                request,
                template_name='sign_in_up.html',
                context={
                    'form': signup_form,
                    'act_as': 'signup',
                }
            )

def logout_user(request):
    logout(request)
    return redirect("login.url")


#tests
def testview(request):
    return HttpResponse("test response")
