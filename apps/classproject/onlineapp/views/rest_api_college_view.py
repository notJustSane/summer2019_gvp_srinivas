from rest_framework.decorators import api_view, authentication_classes, permission_classes

from onlineapp.models import College
from onlineapp.serialize import CollegeSerializer
from rest_framework import status

from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

@api_view(['GET','POST'])
@authentication_classes((SessionAuthentication,TokenAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
def api_college_fun_view(request):
    if request.method == 'GET':
        colleges=College.objects.all()
        serialize_result=CollegeSerializer(colleges,many=True)
        return Response(serialize_result.data)

    elif request.method == 'POST':
        serialize_result = CollegeSerializer(data=request.data)
        if serialize_result.is_valid():
            serialize_result.save()
            return Response(serialize_result.data,status=status.HTTP_201_CREATED)
        return Response(serialize_result.errors,status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET','POST','put','DELETE'])
@authentication_classes((SessionAuthentication, BasicAuthentication, TokenAuthentication))
@permission_classes((IsAuthenticated,))
def api_college_detail_fun_view(request,*args,**kwargs):
    if request.method == 'GET':
        college=College.objects.get(**kwargs)
        serialize_result=CollegeSerializer(college,many=False)
        return Response(serialize_result.data)

    elif request.method == 'PUT':
        college=College.objects.get(**kwargs)
        serialize_result = CollegeSerializer(college,data=request.data)
        if serialize_result.is_valid():
            temp=serialize_result.save()
            return Response(serialize_result.data, status=status.HTTP_201_CREATED)
        return Response(serialize_result.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        College.objects.get(**kwargs).delete()
        return Response("college deleted")
