
from onlineapp.models import Student, MockTest1
from onlineapp.serialize import StudentSerializer, MarksSerializer, StudentMockSerializer
from rest_framework import status

from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

class Api_student_class_view(APIView):
    # def get(self,request,*args,**kwargs):
    #     students=Student.objects.filter(**kwargs)
    #     serialize_result=StudentSerializer(students,many=True)
    #     for i in range(len(students)):
    #         marks=MockTest1.objects.get(student_id=students[i])
    #         marks_serialize=MarksSerializer(marks)
    #         serialize_result.data[i]['mocktext1']=marks_serialize.data
    #         i+=1
    #     return Response(serialize_result.data)


    #checking permissions for this api call

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self,request,*args,**kwargs):
        students=Student.objects.filter(**kwargs)
        serializer_result=StudentMockSerializer(students,many=True)
        return Response(serializer_result.data)

    def post(self,request,*args,**kwargs):
        student_serialize = StudentSerializer(data=request.data)
        if not student_serialize.is_valid():
            return Response(student_serialize.errors, status=status.HTTP_400_BAD_REQUEST)
        temp=student_serialize.save()
        marks_dict = request.data['mocktest1']
        marks_dict['total']=marks_dict['problem1']+marks_dict['problem2']+marks_dict['problem3']+marks_dict['problem4']
        marks_dict['student'] = temp.id
        marks_serialize=MarksSerializer(data=marks_dict)
        if marks_serialize.is_valid():
            marks_serialize.save()
            return Response(marks_serialize.data, status=status.HTTP_201_CREATED)
        return Response(marks_serialize.errors, status=status.HTTP_400_BAD_REQUEST)

class Api_student_edit_class_view(APIView):

    #Checking permissions for this api call

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self,request,*args,**kwargs):
        student = Student.objects.get(id=kwargs['student_id'])
        serializer_result = StudentMockSerializer(student,many=False)
        return Response(serializer_result.data)

    def put(self,request,*args,**kwargs):
        student = Student.objects.get(id=kwargs['student_id'])
        student_serialize =StudentSerializer(student,data=request.data)
        if not student_serialize.is_valid():
            return Response(student_serialize.errors, status=status.HTTP_400_BAD_REQUEST)
        temp=student_serialize.save()
        marks_dict = request.data['mocktest1']
        marks_dict['total'] = marks_dict['problem1'] + marks_dict['problem2'] + marks_dict['problem3'] + marks_dict[
            'problem4']
        marks_dict['student'] = temp.id
        marks=MockTest1.objects.get(student_id=kwargs['student_id'])
        marks_serialize = MarksSerializer(marks,data=marks_dict)
        if marks_serialize.is_valid():
            marks_serialize.save()
            return Response(marks_serialize.data, status=status.HTTP_202_ACCEPTED)
        return Response(marks_serialize.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):

        Student.objects.get(id=kwargs['student_id']).delete()
        return Response("Student Deleted",status=status.HTTP_200_OK)
