from .college import *
from .rest_api_college_view import *
from .rest_api_student_view import *
from .auth import *