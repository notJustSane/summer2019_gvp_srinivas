import click
import openpyxl
import os
import django
from bs4 import BeautifulSoup
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'classproject.settings')
django.setup()
from onlineapp.models import *


#@click.command()
def main():
    wb = openpyxl.load_workbook('students.xlsx')
    college_sheet = wb['Colleges']
    for i in range(2, college_sheet.max_row + 1):
        c=College()
        c.name=college_sheet.cell(row=i,column=1).value.lower()
        c.acronym = college_sheet.cell(row=i, column=2).value.lower()
        c.location = college_sheet.cell(row=i, column=3).value.lower()
        c.contact = college_sheet.cell(row=i, column=4).value.lower()
        c.save()

    student_sheet=wb['Current']
    for i in range(2,student_sheet.max_row+1):
        s=Student()
        s.name=student_sheet.cell(row=i,column=1).value.lower()
        s.college=College.objects.get(acronym=student_sheet.cell(row=i,column=2).value.lower())
        s.email=student_sheet.cell(row=i,column=3).value.lower()
        s.db_folder=student_sheet.cell(row=i,column=4).value.lower()
        s.save()

    #deletions_sheet=wb['Deletions']
    #for i in range(2,deletions_sheet.max_row+1):
    #    if student_sheet.cell(row=i, column=4).value.lower()=="venkatesh":
    #       continue
    #    s = Student()
    #   s.name = student_sheet.cell(row=i, column=1).value.lower()
    #    s.college = College.objects.get(acronym=student_sheet.cell(row=i, column=2).value.lower())
    #    s.email = student_sheet.cell(row=i, column=3).value.lower()
    #    s.db_folder = student_sheet.cell(row=i, column=4).value.lower()
    #    s.dropped_out=True
    #    s.save()

def html():
    file = open('mock_results.html', 'r')
    html_string=file.read()
    soup=BeautifulSoup(html_string,'html.parser')
    for row in soup.find_all('tr'):
        temp=[]
        for cell in row.find_all('td'):
            temp.append(str(cell.string))
        if len(temp)==0:
            continue
        if str(temp[1]).split('_')[2].lower()=='skc':            continue
        m=MockTest1()
        m.student = Student.objects.get(db_folder=str(temp[1]).split('_')[2].lower())
        m.problem1 = int(temp[2])
        m.problem2 = int(temp[3])
        m.problem3 = int(temp[4])
        m.problem4 = int(temp[5])
        m.total = int(temp[6])
        m.save()

if __name__=='__main__':
   html()