
from rest_framework.serializers import ModelSerializer

from onlineapp.models import College, Student, MockTest1


class CollegeSerializer(ModelSerializer):
    class Meta:
        model=College
        fields=('id','name','location','acronym','contact')

class StudentSerializer(ModelSerializer):
    class Meta:
        model=Student
        fields=('id','name','dob','email','db_folder','dropped_out','college')

class MarksSerializer(ModelSerializer):
    class Meta:
        model=MockTest1
        fields=('id','problem1','problem2','problem3','problem4','total','student')

class StudentMockSerializer(ModelSerializer):
    mocktest1=MarksSerializer(many=False,read_only=True)
    class Meta:
        model=Student
        fields=('id','name','dob','email','db_folder','dropped_out','college','mocktest1')


