#include "Header.h"
static char* filename;
static unsigned int blocksize;
static unsigned int nof_blocks;

unsigned int init(char* file_name, unsigned int block_size)
{
	filename = _strdup(file_name);
	blocksize = block_size;
	unsigned int len = findSize(file_name);
	nof_blocks = len/ block_size;
	if (nof_blocks*blocksize != len)
		nof_blocks += 1;
	return nof_blocks;
}

char* read_block(unsigned int block_number)
{
	FILE* fp = fopen(filename, "rb+");
	if (fp == NULL)
		printf("hdd error\n");
	char* buff = (char*)malloc(sizeof(char)*blocksize);
	unsigned int i;
	for (i = 0; i < blocksize; i++)
		buff[i] = '\0';
	buff[i] = '\0';
	fseek(fp, block_number*blocksize, SEEK_SET);
	fread(buff, blocksize, 1, fp);
	fclose(fp);
	return buff;
}


void write_block(char* buff,unsigned int block_number)
{
	FILE* fp = fopen(filename, "rb+");
	if (fp == NULL)
	{
		printf("hard disk failure\n");
		return;
	}
	fseek(fp, blocksize*block_number, SEEK_SET);
	fwrite(buff, blocksize, 1, fp);
	fclose(fp);
}