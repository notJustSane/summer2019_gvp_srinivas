#include "Header.h"
void command_caller_function(char* command, char* command_list[])
{
	unsigned int i;
	char* argument_list[default - 1];
	argument_list[0] = (char*)malloc(sizeof(char)*10);
	argument_list[1] = (char*)malloc(sizeof(char) * 10);
	argument_list[2] = (char*)malloc(sizeof(char) * 10);
	i = 0;
	/*char* token = strtok(command, " ");
	while (token != NULL) {
		argument_list[i++] = _strdup(token);
		token = strtok(NULL, "-");
	}*/
	sscanf(command,"%s %s %s",argument_list[0],argument_list[1],argument_list[2]);
	for (i = 0; i < default; i++)
	{
		if (_stricmp(argument_list[0], command_list[i]) == 0)
		{
			break;
		}
	}
	if (i == 0)
	{
		unsigned int blocksize;
		sscanf(argument_list[2],"%u",&blocksize);
		mount_command(argument_list[1],blocksize);
	}
	else if (i == 1)
	{
		copytofs_command(argument_list[1],argument_list[2]);
	}
	else if (i == 2)
	{
		copyfromfs_command(argument_list[1],argument_list[2]);
	}
	else if (i == 3)
	{
		unsigned int blocksize;
		sscanf(argument_list[1], "%u", &blocksize);
		format_command(blocksize);
	}
	else if (i == 4)
	{
		ls_command();
	}
	else if (i == 5)
	{
		delete_command(argument_list[1]);
	}
	else if (i == 6)
	{
		debug_command();
	}
	else if (i == 7)
	{
		unmount_command();
	}
	else if (i == 8)
	{
		exit(0);
	}
}

int main_hardDisk_command(char* command_list[])
{
	char* command = (char*)malloc(sizeof(char) * 10);
	while (1)
	{
		printf(">");
		gets(command);
		command_caller_function(command, command_list);
	}
	return 0;
}