#include "Header.h"

//This is my metadata which will be maintained in Ram to avoid too many reads for every small operation
//I will write into hdd for each change to make sure nothing is lost

static struct MetaDataStructure meta_data;
static bool mounted;
//This method gives length of any file given in bytes

unsigned int findSize(char* file_name)
{
	FILE* fp = fopen(file_name, "r");
	if (fp == NULL) {
		printf("File Not Found!\n");
		return -1;
	}
	fseek(fp, 0L, SEEK_END);
	unsigned int res = ftell(fp);
	fclose(fp);
	return res - 1;
}

//utility function to read meta_data from HDD

void read_metadata(unsigned int blocksize)
{
	unsigned int blocks_required = sizeof(meta_data)/blocksize;
	if (blocks_required*blocksize != sizeof(meta_data))
		blocks_required += 1;
	char* buffer = (char*)malloc(sizeof(char)*sizeof(meta_data));
	int count = 0;
	while (blocks_required != 0)
	{
		buffer = read_block(count++);
		buffer += blocksize;
		blocks_required--;
	}
	memcpy(&meta_data, buffer, sizeof(meta_data));
}

void write_metadata()
{
	char* buffer = (char*)malloc(sizeof(meta_data));
	memcpy(buffer, &meta_data, sizeof(meta_data));
	int count = 0;
	unsigned int blocks_required = sizeof(meta_data) / meta_data.block_size;
	if (blocks_required*meta_data.block_size != sizeof(meta_data))
		blocks_required += 1;
	while (count != blocks_required)
	{
		write_block(buffer,count++);
		buffer += meta_data.block_size;
	}
}

//this method checks for the availability of required blocks for writing a file or data

unsigned int* check_availability(unsigned int blocksRequired)
{
	unsigned int* res_array = (unsigned int*)malloc(sizeof(unsigned int)*blocksRequired);
	if (meta_data.nof_free_blocks < blocksRequired)
	{
		printf("Not Enough blocks");
		res_array[0] = -1;
		return res_array;
	}
	unsigned int count = 0;
	for (unsigned int i = 4; i < meta_data.nof_blocks-4; i++)
	{
		if (meta_data.byte_vector[i] == 0)
		{
			meta_data.byte_vector[i] = 1;
			res_array[count++] = i;
		}
		if (count == blocksRequired)
			break;
	}
	return res_array;
}


//this method formats all the data in a HDD and asks for new blocksize for maintaining new filesystem

void format_command(unsigned int blocksize)
{
	if (!mounted)
	{
		printf("No HDD is mounted\n");
		return;
	}
	meta_data.block_size = blocksize;
	meta_data.magic_no = MAGIC_NO;
	meta_data.nof_files_read = 0;
}

//this method is used to mount hdd into system and if anything wrong with meta data,it will ask for format

void mount_command(char* file_name,unsigned int blocksize)
{
	unsigned int nof_blocks = init(file_name, blocksize);
	read_metadata(blocksize);
	if (meta_data.magic_no != MAGIC_NO)
	{
		format_command(blocksize);
		unsigned int i;
		for (i = 0; i < nof_blocks; i++)
			meta_data.byte_vector[i] = 0;
		meta_data.nof_blocks = nof_blocks;
		meta_data.nof_free_blocks = nof_blocks;
		write_metadata();
	}
	mounted = true;
}

//this method is used to copy data from external file to HDD

void copytofs_command(char* sourcefile,char* destinationfile)
{
	if (!mounted)
	{
		printf("No HDD is mounted\n");
		return;
	}

	unsigned int length = findSize(sourcefile);
	FILE* fp = fopen(sourcefile, "rb+");
	if (fp == NULL)
		printf("input file returning null\n");
	char* buffer = (char*)malloc(sizeof(char)*length);
	unsigned int i;
	for (i = 0; i < length; i++)
		buffer[i] = '\0';
	fread(buffer, sizeof(char), length, fp);
	fclose(fp);
	unsigned int blocksRequired = length / meta_data.block_size;
	if (blocksRequired*meta_data.block_size != length)
		blocksRequired += 1;
	unsigned int* pos_array = check_availability(blocksRequired);
	if (pos_array[0] < 0)
	{
		printf("no sufficient data available\n");
		return;
	}
	if (blocksRequired > 1)
		blocksRequired += 1;
	//for (i = 0; destinationfile[i] != '\0'; i++)
		//meta_data.file_data[meta_data.nof_files_read].file_name[i] =destinationfile[i];
	memcpy(meta_data.file_data[meta_data.nof_files_read].file_name,destinationfile,strlen(destinationfile));
	meta_data.file_data[meta_data.nof_files_read].file_name[i] = '\0';
	meta_data.file_data[meta_data.nof_files_read].size = length;
	meta_data.file_data[meta_data.nof_files_read].blocks = blocksRequired;
	meta_data.file_data[meta_data.nof_files_read].start = pos_array[0];
	meta_data.nof_files_read += 1;
	meta_data.nof_free_blocks -= 1;
	if (blocksRequired > 1)
	{
		char* temp_buff = (char*)malloc(sizeof(char)*blocksRequired*4);
		memcpy(temp_buff,pos_array,blocksRequired);
		write_block(temp_buff, pos_array[0]);
		for (i = 1; i < blocksRequired; i++)
		{
			write_block(buffer, pos_array[i]);
			meta_data.byte_vector[i] = 1;
			buffer += meta_data.block_size;
		}
	}
	write_metadata();
}

//This method copies data from HDD to external file

void copyfromfs_command(char* sourcefile, char* destinationfile)
{
	if (!mounted)
	{
		printf("No HDD is mounted\n");
		return;
	}
	FILE* fp = fopen(destinationfile, "wb");
	char* buffer;
	
	unsigned int position;
	for (position = 0; position < meta_data.nof_files_read; position++)
	{
		if (_stricmp(sourcefile, meta_data.file_data[position].file_name) == 0)
			break;
	}
	unsigned int start = meta_data.file_data[position].start;
	unsigned int count = 0;
	while (count != meta_data.file_data[position].blocks)
	{
		buffer = read_block(start++);
		count++;
		fwrite(buffer, meta_data.block_size, 1, fp);
	}
	fclose(fp);
}

//This method gives the list of all the files present in the hdd

void ls_command()
{
	if (!mounted)
	{
		printf("No HDD is mounted\n");
		return;
	}
	if (meta_data.nof_files_read == 0)
	{
		printf("HDD is empty\n");
		return;
	}
	printf("Files in this HDD are %d:\n",meta_data.nof_files_read);
	for (unsigned int i = 0; i < meta_data.nof_files_read; i++)
	{
		printf("%s    |    ", meta_data.file_data[i].file_name);
		printf("%d bytes", meta_data.file_data[i].size);
	}
}

//this function displays hdd current details

void debug_command()
{
	if (!mounted)
	{
		printf("No HDD is mounted\n");
		return;
	}
	printf("Details of this Hdd are:\n");
	printf("Block Size:%d\n",meta_data.block_size);
	printf("Number of blocks:%d\n", meta_data.nof_blocks);
	ls_command();
}

//This method deletes the given file from HDD

void delete_command(char* filename)
{
	if (!mounted)
	{
		printf("No HDD is mounted\n");
		return;
	}
	unsigned int position,i;
	for (position = 0; position < meta_data.nof_files_read; position++)
	{
		if (_stricmp(filename, meta_data.file_data[position].file_name) == 0)
			break;
	}
	for (i = meta_data.file_data[position].start; i < meta_data.file_data[position].blocks; i++)
		meta_data.byte_vector[i] = 0;
	meta_data.nof_free_blocks += meta_data.file_data[position].blocks;
	meta_data.file_data[position] = meta_data.file_data[meta_data.nof_files_read - 1];
	meta_data.nof_files_read -= 1;
	write_metadata();
	printf("one file deleted successfully\n");
}

void unmount_command()
{
	mounted = false;
}
