#define _CRT_SECURE_NO_WARNINGS
#define default 9
#define file_name_length 20
#define MAGIC_NO 0x444E524D
#define tablelength 32
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
struct file_record
{
	char file_name[file_name_length];
	unsigned int size;
	unsigned int blocks;
	unsigned int start;
};

struct MetaDataStructure
{
	unsigned int magic_no = MAGIC_NO;
	unsigned int block_size;
	unsigned int nof_blocks;
	unsigned int nof_free_blocks=6400;
	char byte_vector[104858];
	unsigned int nof_files_read=0;
	struct file_record file_data[tablelength];
};
int main_hardDisk_command(char*[]);
char* read_block(unsigned int);
void write_block(char*,unsigned int);
unsigned int init(char*,unsigned int);
unsigned int findSize(char[]);
void mount_command(char*,unsigned int);
void copytofs_command(char*,char*);
void copyfromfs_command(char*, char*);
void format_command(unsigned int);
void ls_command();
void delete_command(char*);
void unmount_command();
void debug_command();