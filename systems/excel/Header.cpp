#include "Header.h"
struct cell* create_cell()
{
	struct cell* new_node = (struct cell*)malloc(sizeof(struct cell));
	return new_node;
}

int parse_argument_to_find_type(char* argument)
{
	char char_count = 0;
	for (int i = 0; argument[i] != '\0'; i++)
	{
		if (argument[i] >= 'a'&&argument[i] <= 'z')
			char_count++;
		if (argument[i] == '+' || argument[i] == '-' || argument[i] == '*' || argument[i] == '/' || argument[i] == '%')
			return 2;
	}
	if (strlen(argument) == char_count)
		return 1;
	else if (char_count == 0)
		return 0;
	else
		return 2;
}

void beautify(char* command)
{
	int i, j;
	for (i = 0, j = 0; command[i] != '\0'; i++)
		if (command[i] != ' ' &&command[i]!='\n')
			command[j++] = command[i];
	command[j] = '\0';
}

int get_int_len(int value){
	int l = 1;
	while (value>9){ l++; value /= 10; }
	return l;
}

int evaluation_stack_helper(int a, int b, char c)
{
	if (c == '+')
		return a + b;
	if (c == '-')
		return a - b;
	if (c == '*')
		return a*b;
	if (c == '/')
		return a / b;
	if (c == '%')
		return a%b;
}

int priority(char oper)
{
	if (oper == '*' || oper == '/')
		return 2;
	else if (oper == '+' || oper == '-')
		return 1;
}

char* create_stack(int max)
{
	char* new_stack = (char*)malloc(sizeof(char)*max);
	for (int i = 0; i < max; i++)
		new_stack[i] = '\0';
	return new_stack;
}

void push(char* stack, char oper,int top)
{
	stack[top] = oper;
}

char pop(char* stack,int top)
{
	char temp = stack[top-1];
	stack[top-1] = '\0';
	return temp;
}

char* infix_to_postfix(char*expression)
{
	char* postfix = (char*)malloc(sizeof(char)*strlen(expression));
	for (int i = 0; i < strlen(expression); i++)
		postfix[i] = '\0';
	int i,j;
	char* stack = create_stack(strlen(expression));
	int top=0;
	for (int i = 0,j=0; expression[i] != '\0';i++)
	{
		postfix[j++] = expression[i];
		if (!(expression[i] >= '0' && expression[i] <= 'z'))
		{
			j--;
			if (expression[i] == '(')
				push(stack,expression[i],top++);
			else if (expression[i] == ')')
			{
				while (stack[top-1]!='(')
				{
					postfix[j++]=pop(stack,top--);
				}
				pop(stack,top--);
			}
			else
			{
				if (top == 0)
					push(stack,expression[i],top++);
				else
				{
					while ((priority(stack[top-1]) <= priority(expression[i]))&&top>0)
					{
						postfix[j++] = pop(stack,top--);
					}
					push(stack, expression[i], top++);
				}
			}
		}
	}
	return postfix;
}

//int evaluate(char* expression,struct cell* current_sheet[][default])
//{
//	char* temp_data = create_stack(strlen(expression));
//	char* oper_stack = create_stack(strlen(expression));
//	int* data_stack = (int*)malloc(sizeof(int)*strlen(expression));
//	int i, data_top = 0, oper_top = 0;
//	for (i = 0; i < expression[i] != '\0'; i++)
//	{
//		data_stack[i] = 0;
//		oper_stack[i] = '\0';
//		temp_data[i] = '\0';
//	}
//	int j = 0;
//	for (i = 0; expression[i] != '\0'; i++)
//	{
//		if ((expression[i] >= '0') && (expression[i] <= 'z'))
//		{
//			temp_data[j++] = expression[i];
//		}
//		else if (!((expression[i] >= '0') && (expression[i] <= 'z'))||strlen(temp_data)!=0)
//		{
//			if (j != 0)
//			{
//				temp_data[j] = '\0';
//				int check_type = parse_argument_to_find_type(temp_data);
//				if (check_type == _int)
//				{
//					int return_number;
//					sscanf(_strdup(expression), "%d", &return_number);
//					data_stack[data_top++] = return_number;
//				}
//				else
//					data_stack[data_top++] = get_command(temp_data, current_sheet, 1);
//				j = 0;
//			}
//			if ((oper_top == 0) || expression[i] == '(' || (priority(expression[i]) > oper_stack[oper_top - 1]))
//				oper_stack[oper_top++] = expression[i];
//			else if (expression[i] == ')')
//			{
//				oper_top--;
//				while (oper_stack[oper_top] != ')')
//				{
//					int a = data_stack[data_top - 1 - 1];
//					int b = data_stack[data_top - 1 - 1 - 1];
//					data_top -= 2;
//					char c = oper_stack[--oper_top];
//					data_stack[data_top++] = evaluation_stack_helper(a, b, c);
//				}
//				oper_top--;
//			}
//			else
//			{
//				while (priority(expression[i]) >= priority(oper_stack[oper_top - 1]))
//				{
//					int a = data_stack[data_top - 1 - 1];
//					int b = data_stack[data_top - 1 - 1 - 1];
//					data_top -= 2;
//					char c = oper_stack[--oper_top];
//					data_stack[data_top++] = evaluation_stack_helper(a, b, c);
//				}
//				oper_stack[oper_top++] = expression[i];
//			}
//		}
//	}
//	if (j != 0)
//	{
//		int check_type = parse_argument_to_find_type(temp_data);
//		if (check_type == _int)
//		{
//			int return_number;
//			sscanf(_strdup(expression), "%d", &return_number);
//			data_stack[data_top++] = return_number;
//		}
//		else
//			data_stack[data_top++] = get_command(temp_data, current_sheet, 1);
//	}
//	oper_top -= 1;
//	data_top -= 1;
//	while (oper_top!=-1)
//	{
//		int a = data_stack[data_top--];
//		int b = data_stack[data_top--];
//		data_stack[++data_top] = evaluation_stack_helper(a,b,oper_stack[oper_top--]);
//	}
//	return data_stack[data_top];
//}

int evaluate(char* expression, struct cell*current_sheet[][default])
{
	int check_type = parse_argument_to_find_type(expression);
	if (check_type == _int)
	{
		int return_number;
		sscanf(_strdup(expression),"%d",&return_number);
		return return_number;
	}
	if (expression[0] == '\0')
		return 0;
	int i;
	char* left_expression = (char*)malloc(sizeof(char)*strlen(expression));
	char* right_expression = (char*)malloc(sizeof(char)*strlen(expression));
	left_expression[0] = '\0';
	right_expression[0] = '\0';
	for (i = 0; expression[i] != '\0'; i++)
	{
		left_expression[i] = expression[i];
		if (expression[i] == '+')
		{
			left_expression[i] = '\0';
			right_expression = _strdup(expression+i+1);
			return evaluate(left_expression,current_sheet) + evaluate(right_expression,current_sheet);
		}
		if (expression[i] == '-')
		{
			left_expression[i] = '\0';
			right_expression = _strdup(expression + i + 1);
			return evaluate(left_expression,current_sheet) - evaluate(right_expression,current_sheet);
		}
		if (expression[i] == '*')
		{
			left_expression[i] = '\0';
			right_expression = _strdup(expression + i + 1);
			return evaluate(left_expression,current_sheet) * evaluate(right_expression,current_sheet);
		}
		if (expression[i] == '/')
		{
			left_expression[i] = '\0';
			right_expression = _strdup(expression + i + 1);
			return evaluate(left_expression,current_sheet) / evaluate(right_expression,current_sheet);
		}
		if (expression[i] == '%')
		{
			left_expression[i] = '\0';
			right_expression = _strdup(expression + i + 1);
			return evaluate(left_expression,current_sheet) % evaluate(right_expression,current_sheet);
		}
	}
	left_expression[i] = '\0';
	return get_command(left_expression,current_sheet,1); //get command acting as evaluator
}

void set_command(char* argument,struct cell* current_sheet[][default])
{
	int i;
	char* row_string = (char*)malloc(sizeof(char)*default);
	for (i = 1; argument[i] != '='; i++)
	{
 		row_string[i - 1] = argument[i];
	}
	row_string[i - 1] = '\0';
	int row_number;
	sscanf(row_string, "%d", &row_number);
	int data_to_be_set;
	int type_check = parse_argument_to_find_type(argument+i+1);
	if (type_check == _int)//j == strlen(argument)              //for setting data
	{
		sscanf(&argument[i + 1], "%d", &data_to_be_set);   
		current_sheet[row_number - 1][argument[0] - 'a']->data = data_to_be_set;
		current_sheet[row_number - 1][argument[0] - 'a']->type = _int;
	}
	else if (type_check == _string)                             //for setting string data
	{
		current_sheet[row_number - 1][argument[0] - 'a']->string = argument+i+1;
		current_sheet[row_number - 1][argument[0] - 'a']->type = _string;

	}
	else if (current_sheet[row_number - 1][argument[0] - 'a']->formula == NULL)                             //for setting formula
	{
		//current_sheet[row_number - 1][argument[0] - 'a']->type = _formula;
		//char* modified_expression = (char*)malloc(sizeof(char)*strlen(argument)+2);
		
		current_sheet[row_number - 1][argument[0] - 'a']->formula = _strdup(argument + i + 1);
	}
}

int get_command(char* argument,struct cell* current_sheet[][default],int act_as_evaluator)
{
	int row_num,returned_value;
	sscanf(argument+1,"%d",&row_num);
	if (current_sheet[row_num - 1][argument[0] - 'a']->formula == NULL)
	{
		if (!act_as_evaluator)
		{
			if (current_sheet[row_num - 1][argument[0] - 'a']->type == _int)
			{
				if (current_sheet[row_num - 1][argument[0] - 'a']->formula != NULL)
					evaluate(current_sheet[row_num - 1][argument[0] - 'a']->formula,current_sheet);
				printf("%d\n", current_sheet[row_num - 1][argument[0] - 'a']->data);
			}
			else if (current_sheet[row_num - 1][argument[0] - 'a']->type==_string)
				printf("%s\n", current_sheet[row_num - 1][argument[0] - 'a']->string);
		}
		return current_sheet[row_num - 1][argument[0] - 'a']->data;
	}
	//set_command(current_sheet[row_num - 1][argument[0] - 'a']->formula,current_sheet);
	returned_value = evaluate(current_sheet[row_num - 1][argument[0] - 'a']->formula, current_sheet);
	char* int_to_string = (char*)malloc(sizeof(char)*get_int_len(returned_value));
	sprintf(int_to_string, "%d", returned_value);
	char* return_str_to_set_cmd = (char*)malloc(sizeof(char)*(sizeof(argument)+1+sizeof(int_to_string)));
	return_str_to_set_cmd[strlen(argument)] = '=';
	int i,j;
	for (i = 0, j = strlen(argument) + 1; int_to_string[i] != '\0'; i++, j++)
		return_str_to_set_cmd[j] = int_to_string[i];
	return_str_to_set_cmd[j] = '\0';
	for (i = 0; argument[i] != '\0'; i++)
		return_str_to_set_cmd[i] = argument[i];
	set_command(return_str_to_set_cmd,current_sheet);
	if (!act_as_evaluator)
	printf("%d\n", current_sheet[row_num - 1][argument[0] - 'a']->data);
	return returned_value;
}

void print_command(char* argument,struct cell* current_sheet[][default])
{
	if (argument[0] == '\0')      //printing full sheet
	{
		for (int i = 0; i < default; i++)
		{
			for (int j = 0; j < default; j++)
			{
				if (current_sheet[i][j]->formula != NULL)
					evaluate(current_sheet[i][j]->formula, current_sheet);
				if (current_sheet[i][j]->type == _int)
					printf("%d", current_sheet[i][j]->data);
				else if (current_sheet[i][j]->type == _string)
					printf("%s", current_sheet[i][j]->string);
				printf("  ");
			}
			printf("\n");
		}
	}
	else if(strcmp(argument,_strdup("header"))==0)                        //printing header
	{
		for (int i = 0, j = 0; j < default; j++)
		{
			if (current_sheet[i][j]->formula != NULL)
				evaluate(current_sheet[i][j]->formula, current_sheet);
			if (current_sheet[i][j]->type == _int)
				printf("%d", current_sheet[i][j]->data);
			else if (current_sheet[i][j]->type == _string)
				printf("%s", current_sheet[i][j]->string);
			printf("  ");
		}
		printf("\n");
	}
}

void import_command(char* argument,struct cell* current_sheet[][default])
{
	FILE* fp = fopen(argument, "r");
	if (fp == NULL)
	{
		printf("Error opening file\n");
		return;
	}
	char* line = (char*)malloc(100*sizeof(char));
	char buff[100];
	int i = 0, j = 0;
	int data;
	int check_type;
	char* token;
	while ((line = fgets(buff, sizeof(buff), fp)) != NULL)
	{
		token = strtok(line, ",");
		while (token != NULL)
		{
			beautify(token);
			check_type = parse_argument_to_find_type(token);
			if (check_type == _int)
			{
				sscanf_s(token, "%d", &data);
				current_sheet[i][j]->data = data;
				current_sheet[i][j]->string = NULL;
				current_sheet[i][j]->type = _int;
				current_sheet[i][j]->formula = NULL;
			}
			else
			{
				current_sheet[i][j]->string = _strdup(token);
				current_sheet[i][j]->data = 0;
				current_sheet[i][j]->type = _string;
				current_sheet[i][j]->formula = NULL;
			}
			j++;
			token = strtok(NULL, ",");
		}
		i++;
		j = 0;
	}
}

void export_command(char* argument, struct cell* current_sheet[][default])
{
	FILE* fp = fopen(argument, "w");
	for (int i = 0; i < default; i++)
	{
		for (int j = 0; j < default; j++)
		{
			fprintf(fp, "%d", current_sheet[i][j]->data);
			if (j < default)
				putc(',', fp);
		}
		putc('\n', fp);
	}
	fclose(fp);
}

void save_command(struct cell*current_sheet[][default],char* imported_name)
{
	if (imported_name[0]=='\0')
	{
		printf("Not imported anything\n");
	}
	else
	{
		export_command(imported_name, current_sheet);
	}
}

void avg_topper_least_command(char*argument,struct cell*current_sheet[][default],int mode)
{
	int i, j;
	int sum = 0, max, min, topper_pos, least_pos;
	if (strlen(argument) == 0)
	{
		for (i = 1; i < default; i++)
		{
			sum = 0;
			for (j = 2; j < default; j++)
				sum += current_sheet[i][j]->data;
			if (i == 1)
			{
				topper_pos = i;
				least_pos = i;
				max = sum;
				min = sum;
			}
			if (sum > max)
			{
				topper_pos = i;
				max = sum;
			}
			if (sum < min)
			{
				least_pos = i;
				min = sum;
			}
		}
		if (mode == 8)
			printf("%s\n", current_sheet[topper_pos][1]->string);
		else
			printf("%s\n", current_sheet[least_pos][1]->string);
		return;
	}
	int row_flag = 0, col_flag = 0;
	for (i = 1, j = 1; i < default; i++)
	{
		if (strcmp(argument, current_sheet[i][j]->string)==0)
		{
			row_flag = 1;
			break;
		}
	}
	if (row_flag)
	{
		for (j = 2; j < default; j++)
			sum += current_sheet[i][j]->data;
		if (mode==7)
			printf("%d\n",sum/(default-2));
		return;
	}
	else
	{
		for (i = 0, j = 2; j < default; j++)
		{
			if (strcmp(argument, current_sheet[i][j]->string)==0)
			{
				col_flag = 1;
				break;
			}
		}
		if (col_flag)
		{
			for (i = 1; i < default; i++)
			{
				sum += current_sheet[i][j]->data;
				if (i == 1)
				{
					topper_pos = i;
					least_pos = i;
					max = current_sheet[i][j]->data;
					min = current_sheet[i][j]->data;
				}
				if (current_sheet[i][j]->data >= max)
				{
					topper_pos = i;
					max = current_sheet[i][j]->data;
				}
				if (current_sheet[i][j]->data <= min)
				{
					least_pos = i;
					min = current_sheet[i][j]->data;
				}
			}
			if (mode == 7)
				printf("%d\n", sum / (default - 1));
			else if (mode == 8)
				printf("%s\n", current_sheet[topper_pos][1]->string);
			else
				printf("%s\n", current_sheet[least_pos ][1]->string);
			return;
		}
		else
			printf("person or subject donot exist\n");
	}
}

int call_command_function(char command[],char command_list[][10],struct cell* current_sheet[][default],char* imported_name)
{
	beautify(command);
	int i, j, argument_pos;
	for (i = 0; i < 10; i++)
		if (strstr(command, command_list[i]) > 0)
		{
			break;
		}
	if (i == 10)
		return -1;
	char* argument = (char*)malloc(sizeof(char) * (strlen(command) - strlen(command_list[i])));
	for (j = strlen(command_list[i]), argument_pos = 0; command[j] != '\0'; j++, argument_pos++)
	{
		argument[argument_pos] = command[j];
	}
	argument[argument_pos] = '\0';
	beautify(argument);
	if (i == 0)
	{
		set_command(argument,current_sheet);
	}
	else if (i == 1)
	{
		get_command(argument,current_sheet,0);
	}
	else if (i == 2)
	{
		print_command(argument,current_sheet);
	}
	else if (i == 3)
	{
		import_command(argument,current_sheet);
		int j;
		for (j = 0; argument[j] != '\0'; j++)
		{
			imported_name[j] = argument[j];
		}
		imported_name[j] = '\0';
	}
	else if (i == 4)
	{
		export_command(argument,current_sheet);
	}
	else if (i == 5)
	{
		save_command(current_sheet,imported_name);
	}
	else if (i == 6)
	{
		exit(0);
	}
	else if (i == 7 || i == 8 || i == 9)
		avg_topper_least_command(argument, current_sheet,i);
	return 0;
}
void initialize_array(struct cell* current_sheet[][default])
{
	for (int i = 0; i < default; i++)
	{
		for (int j = 0; j < default; j++)
		{
			current_sheet[i][j] = create_cell();
			current_sheet[i][j]->data = 0;
			current_sheet[i][j]->type = 0;
			current_sheet[i][j]->formula = NULL;
		}
	}
}
void excel_command(char command_list[][10],struct cell* current_sheet[][default])
{
	char command[100];
	int working_on_imported_file =0;
	char imported_name[100] = {};
	imported_name[0] = '\0';
	while (1)
	{
		printf(">");
		gets_s(command);
		call_command_function(command, command_list, current_sheet,imported_name);
	}
}