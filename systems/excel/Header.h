#define _CRT_SECURE_NO_WARNINGS
#define _int 0
#define _string 1
#define _formula 2
#define default 10
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
struct cell
{
	int data;
	int type;
	char* string = NULL;
	char* formula = NULL;
};
struct cell* create_cell();
void push(char*,char,int);
char pop(char*,int);
void initialize_array(struct cell*[][default]);
void excel_command(char[][10],struct cell*[][default]);
void set_command(char*,struct cell*[][default]);
int get_command(char*,struct cell*[][default],int);
void print_command(char*,struct cell*[][default]);
void import_command(struct cell*[][default]);
void export_command(char*,struct cell*[][default]);
void save_command(struct cell*[][default],char*);
void avg_topper_least_command(char*,struct cell*[][default],int);
void beautify(char*);