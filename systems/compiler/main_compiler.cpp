#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"

char* beautify(char* inp)
{
	char* res = (char*)malloc(sizeof(char)*strlen(inp));
	int i, j;
	for (i = 0,j=0; inp[i] != '\0'; i++)
	{
		if (inp[i] != ' ')
			res[j++] = inp[i];
	}
	res[j] = '\0';
	return res;
}

void data_instruction(char* arguments,struct symbol_table* arr[],int* current_pos,int const_check)
{
	char* dup_string = _strdup(arguments);
	dup_string[1] = '\0';
	arr[*current_pos]->name = dup_string;
	if (*current_pos == 0)
		arr[*current_pos]->address = 0;
	else
		arr[*current_pos]->address = arr[*current_pos-1]->address + arr[*current_pos-1]->size;
	if (arguments[1] == '\0')
	{
		if (!const_check)
			arr[*current_pos]->size = 1;
		else
			arr[*current_pos]->size = 0;
	}
	else
	{
		int size,i;
		char* size_string = _strdup(arguments+2);
		for (i = 0; size_string[i] != ']'; i++);
		size_string[i] = '\0';
		sscanf(size_string,"%d",&size);
		if (!const_check)
			arr[*current_pos]->size = size;
		else
			arr[*current_pos]->size = 0;
	}
	*current_pos += 1;
}

void const_instruction(char* arguments,struct symbol_table* arr[],int* current_pos,int* memory)
{
	char* temp = _strdup(arguments);
	temp[1] = '\0';
	data_instruction(temp, arr, current_pos,1);
	temp = arguments + 2;
	int data;
	sscanf(temp,"%d",&data);
	memory[*current_pos] = data;
	*current_pos += 1;
}

void read_instruction(char* arguments,struct intermediate_language* arr[],int* current_pos,char* register_array[])
{
	if (*current_pos == 0)
		arr[*current_pos]->in_no = 1;
	else
		arr[*current_pos]->in_no = arr[*current_pos-1]->in_no + 1;
	arr[*current_pos]->opcode = READ;
	for (int i = 0; i < NOF_REGISTERS; i++)
	{
		if (_stricmp(register_array[i], arguments)==0)
		{
			arr[*current_pos]->paramaters[0] = i;
				break;
		}
	}
	arr[*current_pos]->paramaters[1] = -1;
	arr[*current_pos]->paramaters[2] = -1;
	arr[*current_pos]->paramaters[3] = -1;
	*current_pos += 1;
}

void mov_instruction(char* arguments[],struct intermediate_language* inter_arr[],struct symbol_table* symbol_arr[],int* current_pos,int opcode,char* register_array[])
{
	if (*current_pos == 0)
		inter_arr[*current_pos]->in_no = 1;
	else 
		inter_arr[*current_pos]->in_no = inter_arr[*current_pos - 1]->in_no + 1;
	inter_arr[*current_pos]->opcode = opcode;
	if (opcode == 1)       //MOV register to memory
	{
		for (int i = 0;; i++)
		{
			if (_stricmp(symbol_arr[i]->name,arguments[0])==0)
			{
				inter_arr[*current_pos]->paramaters[0] = symbol_arr[i]->address;
				break;
			}
		}
		for (int i = 0; i < NOF_REGISTERS; i++)
		{
			if (_stricmp(register_array[i], arguments[1])==0)
			{
				inter_arr[*current_pos]->paramaters[1] = i;
				break;
			}
		}
	}
	else                     //MOV memory to register
	{
		for (int i = 0; i < NOF_REGISTERS; i++)
		{
			if (_stricmp(register_array[i], arguments[0])==0)
			{
				inter_arr[*current_pos]->paramaters[0] = i;
				break;
			}
		}

		for (int i = 0;; i++)
		{
			if (_stricmp(symbol_arr[i]->name, arguments[1])==0)
			{
				inter_arr[*current_pos]->paramaters[1] = symbol_arr[i]->address;
				break;
			}
		}
	}
	inter_arr[*current_pos]->paramaters[2] = -1;
	inter_arr[*current_pos]->paramaters[3] = -1;
	*current_pos+=1;
}

void add_instruction(char* arguments[],struct intermediate_language* inter_array[],struct symbol_table* symbol_arr[],int* current_pos,char* register_array[],int act_as)
{
	if (*current_pos == 0)
		inter_array[*current_pos]->in_no = 1;
	else
		inter_array[*current_pos]->in_no = inter_array[*current_pos - 1]->in_no + 1;
	inter_array[*current_pos]->opcode = act_as + 1;
	for (int i = 0; i < NOF_REGISTERS; i++)
	{
		if (_stricmp(arguments[0], register_array[i])==0)
			inter_array[*current_pos]->paramaters[0] = symbol_arr[i]->address;
		if (_stricmp(arguments[1], register_array[i])==0)
			inter_array[*current_pos]->paramaters[1] = symbol_arr[i]->address;
		if (_stricmp(arguments[2], register_array[i])==0)
			inter_array[*current_pos]->paramaters[2] = symbol_arr[i]->address;
	}
	inter_array[*current_pos]->paramaters[3] = -1;
	*current_pos += 1;
}

void label_encountered(char* label_name, struct label_table* label_arr[],int* current_pos,int count)
{
	label_arr[*current_pos]->block_name = _strdup(label_name);
	label_arr[*current_pos]->address = count;
	*current_pos += 1;
}


void if_encountered(char* arguments[], int* stack, int count,struct intermediate_language* inter_arr[],char* register_array[],int* current_pos,char* instruction_array[])
{
	if (*current_pos == 0)
		inter_arr[*current_pos]->in_no = 0;
	else
		inter_arr[*current_pos]->in_no = inter_arr[*current_pos - 1]->in_no - 1;
	inter_arr[*current_pos]->opcode = IF;
	int i;
	for (i = 0; i < NOF_REGISTERS; i++)
	{
		if (_stricmp(arguments[0], register_array[i]) == 0)
			inter_arr[*current_pos]->paramaters[0] = i;
		if (_stricmp(arguments[2], register_array[i]) == 0)
			inter_arr[*current_pos]->paramaters[2] = i;
	}
	for (i = 0; i < NOF_INSTRUCTIONS; i++)
	{
		if (_stricmp(arguments[1], instruction_array[i]) == 0)
			inter_arr[*current_pos]->paramaters[1] = i;
	}
	*current_pos += 1;
}


void print_instruction(char* argument, struct symbol_table* symbol_arr[],struct intermediate_language* inter_arr[],int* current_pos,char* register_array[])
{
	if (*current_pos == 0)
		inter_arr[*current_pos]->in_no = 1;
	else
		inter_arr[*current_pos]->in_no = inter_arr[*current_pos - 1]->in_no + 1;
	inter_arr[*current_pos]->opcode = PRINT;
	for (int i = 0;; i++)
	{
		if (_stricmp(argument, symbol_arr[i]->name) == 0)
		{
			inter_arr[*current_pos]->paramaters[0] = symbol_arr[i]->address;
			break;
		}
	}
	inter_arr[*current_pos]->paramaters[1] = -1;
	inter_arr[*current_pos]->paramaters[2] = -1;
	inter_arr[*current_pos]->paramaters[3] = -1;
	*current_pos += 1;
}



int instruction_caller_function(char* line, char* instruction_array[], char* register_array[], struct symbol_table* symbol_arr[], struct intermediate_language* intermediate_arr[], struct label_table* label_arr[], int* memory, int* current_symbol, int* current_intermediate, int* current_label)
{
	int i;
	for (i = 0; line[i] == ' '; i++);
	line = line + i;
	char* instruction = (char*)malloc(sizeof(char*) * 5);
	char* arguments = (char*)malloc(sizeof(char*) * 10);
	for (i = 0; line[i] != '\0'; i++)
	{
		if (line[i] == ' ')
			break;
		instruction[i] = line[i];
	}
	instruction[i] = '\0';
	int j, k;
	for (j = i + 1, k = 0; line[j] != '\0'; j++)
	{
		arguments[k++] = line[j];
	}
	arguments[k] = '\0';
	for (i = 0; i < NOF_INSTRUCTIONS; i++)
	{
		if (_stricmp(instruction, instruction_array[i]) == 0)
			break;
	}
	if (i == 0)
	{
		char* operands[2];
		operands[0] = (char*)malloc(sizeof(char) * 4);
		operands[1] = (char*)malloc(sizeof(char) * 4);
		int j;
		for (j = 0; arguments[j] != ','; j++)
			operands[0][j] = arguments[j];
		operands[0][j] = '\0';
		operands[1] = arguments + j + 1;
		operands[0] = beautify(operands[0]);
		operands[1] = beautify(operands[1]);
		int opcode;
		if (operands[0][1] == '\0')
			opcode = 1;
		else
			opcode = 2;
		mov_instruction(operands, intermediate_arr, symbol_arr, current_intermediate, opcode, register_array);
	}
	else if (i == 2 || i == 3 || i==4)
	{
		char* operands[3];
		operands[0] = (char*)malloc(sizeof(char) * 2);
		operands[1] = (char*)malloc(sizeof(char) * 2);
		operands[2] = (char*)malloc(sizeof(char) * 2);
		arguments = beautify(arguments);
		operands[0] = _strdup(arguments);
		operands[0][2] = '\0';
		operands[1] = _strdup(arguments)+3;
		operands[1][2] = '\0';
		operands[2] = _strdup(arguments)+6;
		operands[2][2] = '\0';
		add_instruction(operands,intermediate_arr,symbol_arr,current_intermediate,register_array,i);
	}
	else if (i == 5)
	{
		//jmp_instruction();
	}
	else if (i == 6)
	{
		//if_encountered();
	}
	else if (i == 7)
	{
		//eq_instruction();
	}
	else if (i == 8)
	{
		//lt_instruction();
	}
	else if (i == 9)
	{
		//gt_instruction();
	}
	else if (i == 10)
	{
		//lteq_instruction();
	}
	else if (i == 11)
	{
		//gteq_instruction();
	}
	else if (i == 12)
	{
		arguments = beautify(arguments);
		print_instruction(arguments,symbol_arr,intermediate_arr,current_intermediate,register_array);
	}
	else if (i == 13)
	{
		read_instruction(arguments,intermediate_arr,current_intermediate,register_array);
	}
	else if (i==14)
	{
		data_instruction(arguments,symbol_arr,current_symbol,0);
	}
	else if (i == 15)
	{
		const_instruction(arguments,symbol_arr,current_symbol,memory);
	}
	return 0;
}


int main_compiler(char* instruction_array[],char* register_array[])
{
	struct symbol_table* symbol_arr[100];
	for (int i = 0; i < 100; i++)
		symbol_arr[i] = (struct symbol_table*)malloc(sizeof(struct symbol_table));
	struct intermediate_language* intermediate_arr[100];
	for (int i = 0; i < 100; i++)
		intermediate_arr[i] = (struct intermediate_language*)malloc(sizeof(struct intermediate_language));
	struct label_table* label_arr[100];
	for (int i = 0; i < 100; i++)
		label_arr[i] = (struct label_table*)malloc(sizeof(struct label_table));
	int* memory = (int*)malloc(sizeof(int) * 100);
	int current_symbol = 0;
	int current_intermediate = 0;
	int current_label = 0;

	FILE* fp = fopen("inp.txt","r");
	if (fp == NULL)
	{
		printf("File not found\n");
	}
	char* buff = (char*)malloc(sizeof(char)*20);
	while (fscanf(fp, "%[^\n]s", buff)!=EOF)
	{
		char c = fgetc(fp);
		instruction_caller_function(buff,instruction_array,register_array,symbol_arr,intermediate_arr,label_arr,memory,&current_symbol,&current_intermediate,&current_label);
	}
	for (int i = 0; i < current_intermediate; i++)
	{
		printf("%d,%d,",intermediate_arr[i]->in_no,intermediate_arr[i]->opcode);
		for (int j = 0; j < 4 && intermediate_arr[i]->paramaters[j]!=-1 ; j++)
			printf("%d,", intermediate_arr[i]->paramaters[j]);
		printf("\n");
	}
	fclose(fp);
	return 0;
}