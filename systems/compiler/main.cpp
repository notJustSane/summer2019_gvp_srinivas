#include "Header.h"
int main()
{
	char* instruction_list[NOF_INSTRUCTIONS];
	for (int i = 0; i < NOF_INSTRUCTIONS; i++)
		instruction_list[i] = (char*)malloc(sizeof(char) * 5);
	instruction_list[0] = "mov";
	instruction_list[1][0] = '\0';
	instruction_list[2] = "add";
	instruction_list[3] = "sub";
	instruction_list[4] = "mul";
	instruction_list[5] = "jmp";
	instruction_list[6] = "if";
	instruction_list[7] = "eq";
	instruction_list[8] = "lt";
	instruction_list[9] = "gt";
	instruction_list[10] = "lteq";
	instruction_list[11] = "gteq";
	instruction_list[12] = "print";
	instruction_list[13] = "read";
	instruction_list[14] = "data";
	instruction_list[15] = "const";
	char* register_list[NOF_REGISTERS];
	for (int i = 0; i < NOF_REGISTERS; i++)
		register_list[i] = (char*)malloc(sizeof(char) * 2);
	register_list[0] = "ax";
	register_list[1] = "bx";
	register_list[2] = "cx";
	register_list[3] = "dx";
	register_list[4] = "ex";
	register_list[5] = "fx";
	register_list[6] = "gx";
	register_list[7] = "hx";
	main_compiler(instruction_list,register_list);
	return 0;
}