#define NOF_INSTRUCTIONS 16
#define NOF_REGISTERS 8
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

enum registers
{
	AX,BX,CX,DX,EX,FX,GX,HX
};

enum opcodes
{
	ADD=3,SUB,MUL,JMP,IF,EQ,LT,GT,LTEQ,GTEQ,PRINT,READ
};

struct symbol_table
{
	char* name;
	int address=0;
	int size=0;
};
struct intermediate_language
{
	int in_no=0;
	int opcode=0;
	int paramaters[4];
};
struct label_table
{
	char* block_name;
	int address=0;
};
int main_compiler(char*[], char*[]);